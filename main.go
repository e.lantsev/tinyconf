package main

import (
	"fmt"
	"jejikeh/tinyconf/lexer"
	"os"
)

func main() {
	fmt.Printf("Proccesing file: %s ....\n", os.Args[1])
	raw, err := os.ReadFile(os.Args[1])
	if err != nil {
		panic(err)
	}

	tokens := lexer.New(string(raw), true).Tokenize()
	for _, t := range tokens {
		fmt.Printf("value: %s\t kind: %v\t location: %d\n", t.Value, t.Kind, t.Location)
	}
}
