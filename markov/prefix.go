package markov

import (
	"jejikeh/tinyconf/token"
)

type Prefix []token.TokenType

func (p Prefix) String() string {
	s := ""
	for _, v := range p {
		s += v.String() + " "
	}

	return s
}

// Удаляет из конца один токен и добавляет новый в конец
func (p Prefix) Shift(token token.TokenType) {
	copy(p, p[1:])
	p[len(p)-1] = token
}
