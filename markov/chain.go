package markov

import (
	"jejikeh/tinyconf/token"
	"math/rand"
	"time"
)

type Chain struct {
	chainType       map[string][]token.TokenType
	chainIdentifier map[token.TokenType][]string
	prefixLength    int
	rnd             *rand.Rand
}

func NewChain(prefixLen int) *Chain {
	return &Chain{
		chainType:       make(map[string][]token.TokenType),
		chainIdentifier: make(map[token.TokenType][]string),
		prefixLength:    prefixLen,
		rnd:             rand.New(rand.NewSource(time.Now().Unix())),
	}
}

func (c *Chain) Build(tokens []token.Token) {
	p := make(Prefix, c.prefixLength)
	for _, v := range tokens {
		key := p.String()
		c.chainType[key] = append(c.chainType[key], v.Kind)
		c.chainIdentifier[v.Kind] = append(c.chainIdentifier[v.Kind], v.Value)
		p.Shift(v.Kind)
	}
}

func (c *Chain) GenerateTypeTable(n int) []token.TokenType {
	p := make(Prefix, c.prefixLength)
	var words []token.TokenType
	for i := 0; i < n; i++ {
		choices := c.chainType[p.String()]
		if len(choices) == 0 {
			break
		}
		next := choices[c.rnd.Intn(len(choices))]
		words = append(words, next)
		p.Shift(next)
	}
	return words
}

func (c *Chain) GenerateTypeTableContext(types []token.TokenType, n int) []token.TokenType {
	p := make(Prefix, c.prefixLength)
	p = append(p, types...)
	var words []token.TokenType
	for i := 0; i < n; i++ {
		choices := c.chainType[p.String()]
		if len(choices) == 0 {
			break
		}
		next := choices[c.rnd.Intn(len(choices))]
		words = append(words, next)
		p.Shift(next)
	}
	return words
}

func (c *Chain) GenerateIdentifierTable(tokens []token.TokenType) []string {
	var words []string
	for _, v := range tokens {
		choices := c.chainIdentifier[v]
		if len(choices) == 0 {
			break
		}
		next := choices[c.rnd.Intn(len(choices))]
		words = append(words, next)
	}
	return words
}
