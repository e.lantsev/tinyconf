# tinyconf
`tinyconf` is a small, minimalistic system config programming language. It is designed to be fast and efficient for configuring systems programming, while also being easy to learn and use. With its unique syntax, Tinyconf aims to provide a more intuitive programming experience for developers. It also includes powerful features such as borrow checker, closures, and first-class functions.

# Installation
To install Tinyconf, you will need to have Go installed on your system. You can then use the following command to install tinyconf:

```bash
go get github.com/tinyconf/tinyconf 
```
This will install Tinyconf and its dependencies on your system.

Usage
To use `tinyconf`, simply create a new file with the .tc extension and write your configuration code using the `tinyconf` syntax. You can then run the following command to execute your configuration code:

```bash
tinyconf <filename.tc>
```
This will interpret your `tinyconf` code and execute it. You can also compile your Tinyconf code into a binary using the following command:

```bash
tinyconf build <filename.tc>
```
This will compile your Tinyconf code into a binary that can be executed on your system.

# Syntax
The syntax for `tinyconf` is simple and concise, with a focus on using declarative syntax to define configuration sections and variables. It includes support for variables, expressions, conditionals, loops, and functions, which allows for a more powerful and flexible configuration language.

Here's an example of what the syntax for tinyconf might look like:

```go
// Define variables
max_users = 100;
db_host = "localhost";
db_port = 5432;
db_name = "mydatabase";
db_user = "myuser";
db_password = "mypassword";

// Define a function
fn connect_to_db() -> bool {
  // Connect to the database using the variables defined above
  // Return true if successful, false otherwise
}

// Define a loop that connects to the database
for i := 0; i < max_users; i++ {
  if connect_to_db() {
    // Do something
  } else {
    // Handle error
  }
}

// Define a conditional that sets a feature based on the value of max_users
if max_users > 50 {
  features {
    analytics: true;
    monitoring: true;
  };
} else {
  features {
    analytics: false;
    monitoring: false;
  };
}
```

# Go runtime configuration
`tinycorp` can be used as script language to go applications. 

```go
// hello_world.tc

import "tinyconf/go/print" print(name: string);

fn hello_world() -> () {
  print("hello world");
}

fn export_to_go() -> () {
  => "hello world"
}
```

Example configuration
```go
// main.go

type Conf struct {
  name string
  color u8
}

func main() {
  c := Conf{}
  c = tinycorp.Import("conf.tc", "export_config")  
}

// conf.tc

struct Conf {
  name: string
  color: u8
}

fn export_config() -> (Conf) {
  => new Conf{name: "hello world", color: 0}
}

```



Contributing
If you would like to contribute to `tinyconf`, please feel free to submit a pull request with your changes. We welcome contributions of all types, from bug fixes to new features. If you have any questions or need help getting started, please don't hesitate to reach out to us.

License
`tinyconf` is released under the MIT License. See the LICENSE file for details.