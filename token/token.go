package token

// Это enum с типами токенов
type TokenType uint

const (
	// Syntax tokens
	LeftParen TokenType = iota
	RightParen
	LeftBrace
	RightBrace
	LeftSquareBrace
	RightSquareBrace
	Comma
	Dot
	Minus
	Plus
	Semicolon
	Slash
	Star
	DoubleQuote
	SingleQuote
	Underline
	Colon
	Ampersand

	// this is can be removed
	Tilde
	At
	Hash
	DollarSign
	BackSlash
	Line
	Question
	UpArrow
	Backtick

	// Empty lines. Helpfull for generating good looking code.
	EndLine
	Space
	Tab

	Comment
	// One or two character tokens.
	Bang
	BangEqual
	Equal
	EqualEqual
	TypeEqual
	Greater
	GreaterEqual
	Less
	LessEqual

	Identifier
	String
	Integer
	// Float

	// Keywords.
	// And
	Or
	If
	Else
	// True
	// False
	Fn
	For
	Return
	// While
	Null
	Break
	Continue
	Struct
	Const
	Type

	// Types
	NumericType
	StringType
	LogicalType

	// Declaration
	Package
	Import
)

type Token struct {
	Value    string
	Kind     TokenType
	Location int
}

// Это map для односимвольных токенов.
// В будущем можно добавить и другие токены, такие как if, else, и так далее.
var SyntaxTokenMap = map[string]TokenType{
	// Syntax tokens.
	"(":  LeftParen,
	")":  RightParen,
	"{":  LeftBrace,
	"}":  RightBrace,
	"[":  LeftSquareBrace,
	"]":  RightSquareBrace,
	",":  Comma,
	".":  Dot,
	"-":  Minus,
	"+":  Plus,
	";":  Semicolon,
	"/":  Slash,
	"*":  Star,
	"\"": DoubleQuote,
	"'":  SingleQuote,
	"_":  Underline,
	":":  Colon,
	"&":  Ampersand,

	"~":  Tilde,
	"@":  At,
	"#":  Hash,
	"$":  DollarSign,
	"\\": BackSlash,
	"|":  Line,
	"?":  Question,
	"^":  UpArrow,
	"`":  Backtick,
	// One or two character tokens.
	"//": Comment,
	"!":  Bang,
	"!=": BangEqual,
	"=":  Equal,
	":=": TypeEqual,
	"==": EqualEqual,
	">":  Greater,
	">=": GreaterEqual,
	"<":  Less,
	"<=": LessEqual,

	// Keywords.
	"||":       Or,
	"if":       If,
	"else":     Else,
	"func":     Fn,
	"for":      For,
	"return":   Return,
	"nil":      Null,
	"break":    Break,
	"continue": Continue,
	"struct":   Struct,
	"const":    Const,
	"type":     Type,
	"package":  Package,
	"import":   Import,

	// Types.
	"bool":   LogicalType,
	"string": StringType,
	"int":    NumericType,
}

//go:generate stringer -type=TokenType
