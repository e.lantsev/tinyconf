package lexer

import (
	"fmt"
	"jejikeh/tinyconf/token"
	"strings"
	"unicode"
)

type Lexer struct {
	source        []rune
	cursor        int
	ignore_spaces bool
}

func New(raw string, ignore_spaces bool) *Lexer {
	return &Lexer{
		source:        []rune(raw),
		cursor:        0,
		ignore_spaces: ignore_spaces,
	}
}

func (l *Lexer) Tokenize() []token.Token {
	tokens := []token.Token{}
	token := &token.Token{}

	for l.cursor < len(l.source) {
		token = l.lexWhiteSpace()
		if token != nil && !l.ignore_spaces {
			tokens = append(tokens, *token)
			continue
		}

		if l.cursor == len(l.source) {
			break
		}

		token = l.lexSyntaxToken()
		if token != nil {
			tokens = append(tokens, *token)
			continue
		}

		token = l.lexIntegerToken()
		if token != nil {
			tokens = append(tokens, *token)
			continue
		}

		token = l.lexIdentifierToken()
		if token != nil {
			tokens = append(tokens, *token)
			continue
		}

		f := fmt.Errorf("Unknown token -> %s, detecting as Identifier", string(l.source[l.cursor]))
		fmt.Println(f)
		l.cursor++
	}

	return tokens
}

// lexIdentifierToken ("123 ab + ") => "ab"
// lexIdentifierToken ("123ab + ") => "1234ab"
func (l *Lexer) lexIdentifierToken() *token.Token {
	startCursor := l.cursor
	value := []rune{}

	for l.cursor < len(l.source) {
		ru := l.source[l.cursor]
		if unicode.IsLetter(ru) || (len(value) != 0 && unicode.IsDigit(ru)) {
			value = append(value, ru)
			l.cursor++
			continue
		}

		break
	}

	if len(value) == 0 {
		l.cursor = startCursor
		return nil
	}

	return &token.Token{
		Value:    string(value),
		Kind:     token.Identifier,
		Location: l.cursor,
	}
}

func (l *Lexer) lexIntegerToken() *token.Token {
	startCursor := l.cursor
	value := []rune{}

	for l.cursor < len(l.source) {
		ru := l.source[l.cursor]
		if unicode.IsDigit(ru) {
			value = append(value, ru)
			l.cursor++
			continue
		}

		break
	}

	// If we dont collect any digits, return the start cursor
	if len(value) == 0 {
		l.cursor = startCursor
		return nil
	}

	return &token.Token{
		Value:    string(value),
		Kind:     token.Integer,
		Location: l.cursor,
	}
}

func (l *Lexer) lexSyntaxToken() *token.Token {
	c := string(l.source[l.cursor])
	save_cursor := l.cursor
	var save_token *token.Token

	if t, ok := token.SyntaxTokenMap[c]; ok {
		// Парсинг строки. Если потом нужно будет - перенести в отдельный метод
		if (t == token.DoubleQuote || t == token.SingleQuote) && l.cursor+1 < len(l.source) {
			start_cursor := l.cursor
			string_value := strings.Builder{}
			for l.cursor < len(l.source) {
				string_value.WriteRune(l.source[l.cursor])
				l.cursor++

				if l.cursor == len(l.source) {
					break
				}

				if temp_t, ok := token.SyntaxTokenMap[string(l.source[l.cursor])]; ok {
					// Это может не рабоать на windows, так как там для обозначения перехода
					// на новую строку иногда добавляется \r
					if temp_t == t || l.cursor+1 == len(l.source) {
						string_value.WriteRune(l.source[l.cursor])
						l.cursor++
						return &token.Token{
							Value:    strings.TrimSpace(string_value.String()),
							Kind:     token.String,
							Location: l.cursor,
						}
					}
				}
			}

			l.cursor = start_cursor
		}

		if l.cursor+2 < len(l.source) {
			//  Для токенов, которые состоят из двух символов и начинаются с существующего токена
			// К примеру => начинется с =, а этот токен существует
			if n, ok := token.SyntaxTokenMap[string(l.source[l.cursor:l.cursor+2])]; ok {
				// Парсинг комментов. Пока добавляются в массив токенов, и наверное будут и дальше.
				// Комменты в могут помочь нам анализировать код в далеком будущем
				// Более подробно про данный момент можно прочитать в token.go
				if n == token.Comment && l.cursor+1 < len(l.source) {
					start_cursor := l.cursor
					comment := strings.Builder{}

					for l.cursor+1 < len(l.source) {
						if l.source[l.cursor+1] == '\n' || l.cursor+1 == len(l.source) {
							comment.WriteRune(l.source[l.cursor])
							l.cursor++
							return &token.Token{
								Value:    strings.TrimSpace(comment.String()),
								Kind:     token.Comment,
								Location: l.cursor,
							}
						}

						comment.WriteRune(l.source[l.cursor])
						l.cursor++
					}

					l.cursor = start_cursor
				}
			}
		}

		// Для односимвольных токенов
		l.cursor++
		save_token = &token.Token{
			Value:    c,
			Kind:     t,
			Location: l.cursor,
		}
	} else {
		l.cursor++
	}

	for i := 0; i < 8; i++ {
		if t := l.lexSyntaxTokenNLength(i); t != nil {
			save_token = t
		}
	}

	if save_token == nil {
		l.cursor = save_cursor
	}

	return save_token
}

func (l *Lexer) lexSyntaxTokenNLength(length int) *token.Token {
	var save_token *token.Token

	if l.cursor-1 >= 0 && l.cursor+length <= len(l.source) {
		if n, ok := token.SyntaxTokenMap[string(l.source[l.cursor-1:l.cursor+length])]; ok {
			l.cursor += length
			save_token = &token.Token{
				Value:    string(l.source[l.cursor-length-1 : l.cursor]),
				Kind:     n,
				Location: l.cursor,
			}
		}
	}

	return save_token
}

func (l *Lexer) lexWhiteSpace() *token.Token {
	for l.cursor < len(l.source) {
		ru := l.source[l.cursor]
		if unicode.IsSpace(ru) {
			l.cursor++

			if !l.ignore_spaces {
				if ru == '\n' {
					return &token.Token{
						Value:    "\n",
						Kind:     token.EndLine,
						Location: l.cursor,
					}
				}

				if ru == ' ' {
					return &token.Token{
						Value:    " ",
						Kind:     token.Space,
						Location: l.cursor,
					}
				}

				if ru == '\t' {
					return &token.Token{
						Value:    "\t",
						Kind:     token.Tab,
						Location: l.cursor,
					}
				}

			}

			continue
		}

		break
	}

	return nil
}
