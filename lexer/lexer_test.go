package lexer

import (
	"fmt"
	"jejikeh/tinyconf/token"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_LexIdentifierToken(t *testing.T) {
	tests := []struct {
		Source         string
		Cursor         int
		ExpectedValue  string
		ExpectedCursor int
	}{
		{
			"foo 123",
			0,
			"foo",
			3,
		},
		{
			"23 da",
			3,
			"da",
			5,
		},
		{
			"223123 3 f ",
			9,
			"f",
			10,
		},
		{
			"2 f3",
			2,
			"f3",
			4,
		},
		{
			"123 ab ",
			4,
			"ab",
			6,
		},
	}

	for _, test := range tests {
		l := New(test.Source, true)
		l.cursor = test.Cursor
		tok := l.lexIdentifierToken()

		fmt.Printf("expected_value: %s, got_value: %s\n", test.ExpectedValue, tok.Value)
		fmt.Printf("expected_cursor: %d, got_cursor: %d\n", test.ExpectedCursor, l.cursor)
		fmt.Printf("expected_kind: %d, got_kind: %d\n", token.Identifier, tok.Kind)

		assert.Equal(t, test.ExpectedValue, tok.Value)
		assert.Equal(t, test.ExpectedCursor, l.cursor)
		assert.Equal(t, token.Identifier, tok.Kind)
	}
}

func Test_LexIntegerToken(t *testing.T) {
	tests := []struct {
		Source         string
		Cursor         int
		ExpectedValue  string
		ExpectedCursor int
	}{
		{
			"foo 123",
			4,
			"123",
			7,
		},
		{
			"23",
			0,
			"23",
			2,
		},
		{
			"fo 223123 3",
			3,
			"223123",
			9,
		},
		{
			"fo 2 3",
			5,
			"3",
			6,
		},
	}

	for _, test := range tests {
		l := New(test.Source, true)
		l.cursor = test.Cursor
		tok := l.lexIntegerToken()

		fmt.Printf("expected_value: %s, got_value: %s\n", test.ExpectedValue, tok.Value)
		fmt.Printf("expected_cursor: %d, got_cursor: %d\n", test.ExpectedCursor, l.cursor)
		fmt.Printf("expected_kind: %d, got_kind: %d\n", token.Integer, tok.Kind)

		assert.Equal(t, test.ExpectedValue, tok.Value)
		assert.Equal(t, test.ExpectedCursor, l.cursor)
		assert.Equal(t, token.Integer, tok.Kind)
	}
}

func Test_LexSyntaxToken(t *testing.T) {
	tests := []struct {
		Source            string
		Cursor            int
		ExpectedValue     string
		ExpectedCursor    int
		ExpectedTokenType token.TokenType
	}{
		{
			"fo(o 123",
			2,
			"(",
			3,
			token.LeftParen,
		},
		{
			"23)",
			2,
			")",
			3,
			token.RightParen,
		},
		{
			"{fo 223123 3",
			0,
			"{",
			1,
			token.LeftBrace,
		},
		{
			"fo }2 3",
			3,
			"}",
			4,
			token.RightBrace,
		},
		{
			"fo,o 123",
			2,
			",",
			3,
			token.Comma,
		},
		{
			"23.",
			2,
			".",
			3,
			token.Dot,
		},
		{
			"-fo 223123 3",
			0,
			"-",
			1,
			token.Minus,
		},
		{
			"fo +2 3",
			3,
			"+",
			4,
			token.Plus,
		},
		{
			"23;",
			2,
			";",
			3,
			token.Semicolon,
		},
		{
			"/fo 223123 3",
			0,
			"/",
			1,
			token.Slash,
		},
		{
			"fo *2 3//",
			3,
			"*",
			4,
			token.Star,
		},
		{
			"fo =2 3//",
			3,
			"=",
			4,
			token.Equal,
		},
	}

	for _, test := range tests {
		l := New(test.Source, true)
		l.cursor = test.Cursor
		tok := l.lexSyntaxToken()

		fmt.Printf("expected_value: %s, got_value: %s\n", test.ExpectedValue, tok.Value)
		fmt.Printf("expected_cursor: %d, got_cursor: %d\n", test.ExpectedCursor, l.cursor)
		fmt.Printf("expected_kind: %d, got_kind: %d\n", test.ExpectedTokenType, tok.Kind)

		assert.Equal(t, test.ExpectedValue, tok.Value)
		assert.Equal(t, test.ExpectedCursor, l.cursor)
		assert.Equal(t, test.ExpectedTokenType, tok.Kind)
	}
}

func Test_Lexer(t *testing.T) {
	tests := []struct {
		Source string
		Tokens []token.Token
	}{
		{
			"fo;o 123",
			[]token.Token{
				{Value: "fo", Kind: token.Identifier, Location: 2},
				{Value: ";", Kind: token.Semicolon, Location: 3},
				{Value: "o", Kind: token.Identifier, Location: 4},
				{Value: "123", Kind: token.Integer, Location: 8},
			},
		},
		{
			"1add +",
			[]token.Token{
				{Value: "1", Kind: token.Integer, Location: 1},
				{Value: "add", Kind: token.Identifier, Location: 4},
				{Value: "+", Kind: token.Plus, Location: 6},
			},
		},
		{
			"hello.2142(world;",
			[]token.Token{
				{Value: "hello", Kind: token.Identifier, Location: 5},
				{Value: ".", Kind: token.Dot, Location: 6},
				{Value: "2142", Kind: token.Integer, Location: 10},
				{Value: "(", Kind: token.LeftParen, Location: 11},
				{Value: "world", Kind: token.Identifier, Location: 16},
				{Value: ";", Kind: token.Semicolon, Location: 17},
			},
		},
	}

	for _, test := range tests {
		l := New(test.Source, true)
		tokens := l.Tokenize()
		assert.Equal(t, test.Tokens, tokens)
	}
}
