package parser

import (
	"jejikeh/tinyconf/token"
)

func Parse(tokens []token.Token, index int) (AstTree, int) {
	var a AstTree
	for index < len(tokens) {
		if tokens[index].Kind == token.RightBrace {
			child, nextIndex := Parse(tokens, index+1)
			a = append(a, child)
			index = nextIndex
		}
	}
}
