package parser

import "jejikeh/tinyconf/token"

// TODO(jejikeh): Дописать и улыбнуться
// Это абстрактоное синтаксическое дерево. Его короче
// создает парсер. Это дерево хранит в себе в древовидной структуре
// всю программу. А это значит, что можно взять, и транслировать АСТ дерево
// с одного языка, на другой
type AstTree []AstToken

// Выглядеть оно может приблизительно вот так
//        +
//   +        -
// 12  2    12  1

// Это для выражение =>
// (12 + 2) + (12 - 1);

// TODO(jejikeh): допишите пожалуйста про репрезентацию АСТ :)
// Значит, лексер вывалил нам токены и их типы, а дальше дело парсера построить АСТ
// Так как же будет выглядеть это АСТ? Примерно так ->
// AstToken {
// 	kind  List
// 	value "(12 + 2)"
// 	list: []AstToken {
// 		AstToken {
// 			kind Literal
// 			value "12" -> на самом деле тут будет лежать не строка, а Token
// 			list nil
// 		}
//		AstToken {
//			kind Operation
//		}
//}
// 	}
// }

type AstTokenType uint

const (
	Literal AstTokenType = iota
	List
)

type AstToken struct {
	Kind    AstTokenType
	Literal *token.Token
	List    *AstTree
}
